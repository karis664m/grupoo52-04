/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.GrupoO5204.GrupoO5204.Servicios;

import com.GrupoO5204.GrupoO5204.Dao.diagnosticoDao;
import com.GrupoO5204.GrupoO5204.modelos.diagnostico;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author dmigu
 */

@Service
public class DiagnosticoServicio {
    @Autowired
    private diagnosticoDao diagnosticoDao;
    
    public List<diagnostico> ListarDiagnosticos (){
        return diagnosticoDao.findAll();
        
    }
}

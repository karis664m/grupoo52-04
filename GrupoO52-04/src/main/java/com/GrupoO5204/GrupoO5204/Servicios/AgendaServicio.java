/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.GrupoO5204.GrupoO5204.Servicios;

import com.GrupoO5204.GrupoO5204.Dao.AgendaDao;
import com.GrupoO5204.GrupoO5204.modelos.Agenda;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author Andres Vargas
 */

@Service
public class AgendaServicio {
    
    @Autowired
    private AgendaDao agendaDao;
    
    public List<Agenda> listarAgendas (){
        return agendaDao.findAll();
    }
    
    public Agenda insertarAgenda(Agenda agendaNueva){
        return agendaDao.save(agendaNueva);
    }
    
    public void eliminarAgenda(int idAgenda){
        agendaDao.delete(agendaDao.findById(idAgenda).get());
    }
    
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.GrupoO5204.GrupoO5204.Servicios;

import com.GrupoO5204.GrupoO5204.Dao.AdministracionDao;
import com.GrupoO5204.GrupoO5204.modelos.Administracion;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author Andres Vargas
 */

@Service
public class AdministracionServicio {
    @Autowired
    private AdministracionDao administracionDao;
    
    public List<Administracion> listarAdministracion (){
        return administracionDao.findAll();
    }
    
    public Administracion insertarAgenda(Administracion administracionNueva){
        return administracionDao.save(administracionNueva);
    }
    
    public void eliminarAdministracion(int idUsuario){
        administracionDao.delete(administracionDao.findById(idUsuario).get());
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.GrupoO5204.GrupoO5204.Servicios;

import com.GrupoO5204.GrupoO5204.Dao.CitapacienteDao;
import com.GrupoO5204.GrupoO5204.modelos.Citapaciente;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author Andres Vargas
 */

@Service
public class CitapacienteServicio {
    @Autowired
    private CitapacienteDao citapacienteDao;
    
    public List<Citapaciente> listarCitaspaciente (){
        return citapacienteDao.findAll();
    }
    
    public Citapaciente insertarCitaspaciente(Citapaciente citapacienteNueva){
        return citapacienteDao.save(citapacienteNueva);
    }
    
    public void eliminarCitaspaciente(int idCita){
        citapacienteDao.delete(citapacienteDao.findById(idCita).get());
    }
}

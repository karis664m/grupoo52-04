/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.GrupoO5204.GrupoO5204.Servicios;

import com.GrupoO5204.GrupoO5204.Dao.ProfesionalDao;
import com.GrupoO5204.GrupoO5204.modelos.Profesional;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author Andres Vargas
 */

@Service
public class ProfesionalServicio {
    @Autowired
    private ProfesionalDao profesionalDao;
    
    public List<Profesional> listarProfesionales (){
        return profesionalDao.findAll();
    }
    
    public Profesional insertarProfesional(Profesional profesionalNuevo){
        return profesionalDao.save(profesionalNuevo);
    }
    
    public void eliminarProfesional(int idPacinte){
        profesionalDao.delete(profesionalDao.findById(idPacinte).get());
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.GrupoO5204.GrupoO5204.Servicios;

/**
 *
 * @author Andres Vargas
 */

import com.GrupoO5204.GrupoO5204.Dao.PacienteDao;
import com.GrupoO5204.GrupoO5204.modelos.Paciente;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service


public class PacienteServicio {
    @Autowired
    private PacienteDao pacienteDao;
    
    public List<Paciente> listarPacientes (){
        return pacienteDao.findAll();
    }
    
    public Paciente insertarPaciente(Paciente pacienteNuevo){
        return pacienteDao.save(pacienteNuevo);
    }
    
    public void eliminarPaciente(int idPacinte){
        pacienteDao.delete(pacienteDao.findById(idPacinte).get());
    }
}

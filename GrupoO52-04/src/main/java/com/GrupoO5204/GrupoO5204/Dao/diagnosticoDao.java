/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package com.GrupoO5204.GrupoO5204.Dao;

import com.GrupoO5204.GrupoO5204.modelos.diagnostico;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 *
 * @author dmigu
 */
public interface diagnosticoDao extends JpaRepository<diagnostico, Integer> {
    
}

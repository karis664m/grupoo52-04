/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.GrupoO5204.GrupoO5204.Dao;

/**
 *
 * @author Andres Vargas
 */

import com.GrupoO5204.GrupoO5204.modelos.Paciente;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PacienteDao extends JpaRepository<Paciente, Integer> {
    
}

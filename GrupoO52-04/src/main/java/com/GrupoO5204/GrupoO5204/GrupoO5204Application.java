package com.GrupoO5204.GrupoO5204;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class GrupoO5204Application {

	public static void main(String[] args) {
		SpringApplication.run(GrupoO5204Application.class, args);
	}

}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.GrupoO5204.GrupoO5204.Controladores;

import com.GrupoO5204.GrupoO5204.Servicios.ProfesionalServicio;
import com.GrupoO5204.GrupoO5204.modelos.Profesional;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author Andres Vargas
 */
@RestController
@CrossOrigin("*")
@RequestMapping("/profesional")
public class ProfesionalControlador {
    @Autowired
    private ProfesionalServicio profesionalServicio;
    
    @GetMapping
    public List<Profesional> consultarTodo(){
        return profesionalServicio.listarProfesionales();
    }
    
    @PostMapping
    public Profesional insertar(@RequestBody Profesional profesionalNuevo){
        return profesionalServicio.insertarProfesional(profesionalNuevo);
    }
    
    @DeleteMapping (path="/{idprofesional}")
    public String eliminar (@PathVariable Integer idprofesional){
        try {
            profesionalServicio.eliminarProfesional(idprofesional);
                    return "Eliminado exitosamente";
        } catch (Exception e) {
            return "Error al eliminar"+e.getMessage();
        }
    }
    
    @PutMapping
    public Profesional Actualizar (@RequestBody Profesional profesionalActualizar){
        return profesionalServicio.insertarProfesional(profesionalActualizar);
    }
}

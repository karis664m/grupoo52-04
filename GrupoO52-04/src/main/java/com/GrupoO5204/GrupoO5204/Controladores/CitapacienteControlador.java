/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.GrupoO5204.GrupoO5204.Controladores;

import com.GrupoO5204.GrupoO5204.Servicios.CitapacienteServicio;
import com.GrupoO5204.GrupoO5204.modelos.Citapaciente;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author Andres Vargas
 */

@RestController
@CrossOrigin("*")
@RequestMapping("/cita")
public class CitapacienteControlador {
    @Autowired
    private CitapacienteServicio citapacienteServicio;
    
    @GetMapping
    public List<Citapaciente> consultarTodo (){
        return citapacienteServicio.listarCitaspaciente();
    }
    
    @PostMapping
    public Citapaciente Insertar (@RequestBody Citapaciente citaNueva){
        return citapacienteServicio.insertarCitaspaciente(citaNueva);
    }
    
    @DeleteMapping (path="/{idcita}")
    public String Eliminar (@PathVariable Integer idcita){
        try {
            citapacienteServicio.eliminarCitaspaciente(idcita);
                    return "Eliminado exitosamente";
        } catch (Exception e) {
            return "Error al eliminar"+e.getMessage();
        }
    }
    
    @PutMapping
    public Citapaciente Actualizar (@RequestBody Citapaciente citaActualizar){
        return citapacienteServicio.insertarCitaspaciente(citaActualizar);
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.GrupoO5204.GrupoO5204.Controladores;

import com.GrupoO5204.GrupoO5204.Servicios.AgendaServicio;
import com.GrupoO5204.GrupoO5204.modelos.Agenda;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author Andres Vargas
 */
@RestController
@CrossOrigin("*")
@RequestMapping("/agenda")

public class AgendaControlador {
    
    @Autowired
    private AgendaServicio agendaServicio;
    
    @GetMapping
    public List<Agenda> consultarTodo (){
        return agendaServicio.listarAgendas();
    }
    
    @PostMapping
    public Agenda Insertar (@RequestBody Agenda agendaNueva){
        return agendaServicio.insertarAgenda(agendaNueva);
    }
    
    @DeleteMapping (path="/{idagenda}")
    public String Eliminar (@PathVariable Integer idagenda){
        try {
            agendaServicio.eliminarAgenda(idagenda);
                    return "Eliminado exitosamente";
        } catch (Exception e) {
            return "Error al eliminar"+e.getMessage();
        }
    }
    
    @PutMapping
    public Agenda Actualizar (@RequestBody Agenda agendaActualizar){
        return agendaServicio.insertarAgenda(agendaActualizar);
    }
}

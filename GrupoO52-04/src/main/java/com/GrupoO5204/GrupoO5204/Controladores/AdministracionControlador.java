/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.GrupoO5204.GrupoO5204.Controladores;

import com.GrupoO5204.GrupoO5204.Servicios.AdministracionServicio;
import com.GrupoO5204.GrupoO5204.modelos.Administracion;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author Andres Vargas
 */

@RestController
@CrossOrigin("*")
@RequestMapping("/login")
public class AdministracionControlador {
    @Autowired
    private AdministracionServicio administracionServicio;
    
    @GetMapping
    public List<Administracion> consultarTodo (){
        return administracionServicio.listarAdministracion();
    }
    
    @PostMapping ("/registro")
    public Administracion Insertar (@RequestBody Administracion administracionNueva){
        return administracionServicio.insertarAgenda(administracionNueva);
    }
    
    @DeleteMapping (path="/{idusuario}")
    public String Eliminar (@PathVariable Integer idusuario){
        try {
            administracionServicio.eliminarAdministracion(idusuario);
                    return "Eliminado exitosamente";
        } catch (Exception e) {
            return "Error al eliminar"+e.getMessage();
        }
    }
    
    @PutMapping
    public Administracion Actualizar (@RequestBody Administracion agendaActualizar){
        return administracionServicio.insertarAgenda(agendaActualizar);
    }
}

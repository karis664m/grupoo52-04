/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.GrupoO5204.GrupoO5204.Controladores;

import com.GrupoO5204.GrupoO5204.Servicios.PacienteServicio;
import com.GrupoO5204.GrupoO5204.modelos.Paciente;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author Andres Vargas
 */
@RestController
@CrossOrigin("*")
@RequestMapping("/paciente")

public class PacienteControlador {
    
    @Autowired
    private PacienteServicio pacienteServicio;
    
    @GetMapping("/lista")
    public List<Paciente> listadoPacientes(){
        return pacienteServicio.listarPacientes();
    }
    
    @PostMapping
    public Paciente insertar(@RequestBody Paciente pacienteNuevo){
        return pacienteServicio.insertarPaciente(pacienteNuevo);
    }
    
    @DeleteMapping (path="/{idpaciente}")
    public String eliminar (@PathVariable Integer idpaciente){
        try {
            pacienteServicio.eliminarPaciente(idpaciente);
                    return "Eliminado exitosamente";
        } catch (Exception e) {
            return "Error al eliminar"+e.getMessage();
        }
    }
    
    @PutMapping
    public Paciente Actualizar (@RequestBody Paciente pacienteActualizar){
        return pacienteServicio.insertarPaciente(pacienteActualizar);
    }
    
}

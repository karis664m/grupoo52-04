/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.GrupoO5204.GrupoO5204.Controladores;

import com.GrupoO5204.GrupoO5204.Servicios.DiagnosticoServicio;
import com.GrupoO5204.GrupoO5204.modelos.diagnostico;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author dmigu
 */

@RestController
@CrossOrigin("*")
@RequestMapping("/diagnostico")

public class DiagnosticoControlador {
    @Autowired
    private DiagnosticoServicio diagnosticoServicio;
    @GetMapping
    public List<diagnostico> consultarTodo(){
        return diagnosticoServicio.ListarDiagnosticos();
    }
    


    
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.GrupoO5204.GrupoO5204.modelos;

import java.io.Serializable;
import java.sql.Time;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 *
 * @author Andres Vargas
 */

@Table
@Entity(name="profesional")

public class Profesional implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="idprofesional")
    private Integer idProfesional;
    
    @Column(name="identificacionmedico")
    private String identificacionMedico;
    
    @Column(name="tipoidmedico")
    private String tipoidMedico;
    
    @Column(name="registromedico")
    private String registroMedico;
    
    @Column(name="nombremedico")
    private String nombreMedico;
    
    @Column(name="apellidomedico")
    private String apellidoMedico;
    
    @Column(name="especialidad")
    private String especialidad;
    
    @Column(name="telefonomedico")
    private String telefonoMedico;
    
    @Column(name="mailmedico")
    private String mailMedico;
    
    @Column(name="fechadisponible")
    private Date fechaDisponible;
    
    @Column(name="horadisponible")
    private Time horaDisponible;

    public Integer getIdProfesional() {
        return idProfesional;
    }

    public void setIdProfesional(Integer idProfesional) {
        this.idProfesional = idProfesional;
    }

    public String getIdentificacionMedico() {
        return identificacionMedico;
    }

    public void setIdentificacionMedico(String identificacionMedico) {
        this.identificacionMedico = identificacionMedico;
    }

    public String getTipoidMedico() {
        return tipoidMedico;
    }

    public void setTipoidMedico(String tipoidMedico) {
        this.tipoidMedico = tipoidMedico;
    }

    public String getRegistroMedico() {
        return registroMedico;
    }

    public void setRegistroMedico(String registroMedico) {
        this.registroMedico = registroMedico;
    }

    public String getNombreMedico() {
        return nombreMedico;
    }

    public void setNombreMedico(String nombreMedico) {
        this.nombreMedico = nombreMedico;
    }

    public String getApellidoMedico() {
        return apellidoMedico;
    }

    public void setApellidoMedico(String apellidoMedico) {
        this.apellidoMedico = apellidoMedico;
    }

    public String getEspecialidad() {
        return especialidad;
    }

    public void setEspecialidad(String especialidad) {
        this.especialidad = especialidad;
    }

    public String getTelefonoMedico() {
        return telefonoMedico;
    }

    public void setTelefonoMedico(String telefonoMedico) {
        this.telefonoMedico = telefonoMedico;
    }

    public String getMailMedico() {
        return mailMedico;
    }

    public void setMailMedico(String mailMedico) {
        this.mailMedico = mailMedico;
    }

    public Date getFechaDisponible() {
        return fechaDisponible;
    }

    public void setFechaDisponible(Date fechaDisponible) {
        this.fechaDisponible = fechaDisponible;
    }

    public Time getHoraDisponible() {
        return horaDisponible;
    }

    public void setHoraDisponible(Time horaDisponible) {
        this.horaDisponible = horaDisponible;
    }
    
    
}

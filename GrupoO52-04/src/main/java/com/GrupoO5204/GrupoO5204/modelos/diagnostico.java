/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.GrupoO5204.GrupoO5204.modelos;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 *
 * @author dmigu
 */
@Table
@Entity (name="diagnostico")
public class diagnostico implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="iddiagnostico")
    private Integer idDiagnostico;
    
    @Column(name="codigodx")
    private String codigoDx;
    
    @Column(name="descripciondx")
    private String descripcionDx;

    public Integer getIdDiagnostico() {
        return idDiagnostico;
    }

    public void setIdDiagnostico(Integer idDiagnostico) {
        this.idDiagnostico = idDiagnostico;
    }

    public String getCodigoDx() {
        return codigoDx;
    }

    public void setCodigoDx(String codigoDx) {
        this.codigoDx = codigoDx;
    }

    public String getDescripcionDx() {
        return descripcionDx;
    }

    public void setDescripcionDx(String descripcionDx) {
        this.descripcionDx = descripcionDx;
    }
    
    
}

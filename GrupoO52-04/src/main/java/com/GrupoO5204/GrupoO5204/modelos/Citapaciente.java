/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.GrupoO5204.GrupoO5204.modelos;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

/**
 *
 * @author Andres Vargas
 */

@Table
@Entity(name="citapaciente")
public class Citapaciente implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="idcita")
    private Integer idCita;
    
    @ManyToOne
    @JoinColumn(name="idmedico")
    private Profesional profesional;
    
    @OneToOne
    @JoinColumn(name="diagnostico")
    private diagnostico diagnostico;
    
    @Column(name="motivoconsulta")
    private String motivoConsulta;
    
    @Column(name="formulacion")
    private String Formulacion;
    
    @Column(name="observacion")
    private String Observacion;
    
    @Column(name="fecharegistro")
    private Date fechaRegistro;

    public Integer getIdCita() {
        return idCita;
    }

    public void setIdCita(Integer idCita) {
        this.idCita = idCita;
    }

    public Profesional getProfesional() {
        return profesional;
    }

    public void setProfesional(Profesional profesional) {
        this.profesional = profesional;
    }

    public diagnostico getDiagnostico() {
        return diagnostico;
    }

    public void setDiagnostico(diagnostico diagnostico) {
        this.diagnostico = diagnostico;
    }

    public String getMotivoConsulta() {
        return motivoConsulta;
    }

    public void setMotivoConsulta(String motivoConsulta) {
        this.motivoConsulta = motivoConsulta;
    }

    public String getFormulacion() {
        return Formulacion;
    }

    public void setFormulacion(String Formulacion) {
        this.Formulacion = Formulacion;
    }

    public String getObservacion() {
        return Observacion;
    }

    public void setObservacion(String Observacion) {
        this.Observacion = Observacion;
    }

    public Date getFechaRegistro() {
        return fechaRegistro;
    }

    public void setFechaRegistro(Date fechaRegistro) {
        this.fechaRegistro = fechaRegistro;
    }
    
    
}
